CMAKE_MINIMUM_REQUIRED(VERSION 2.8.7)
PROJECT (TENIS)
FIND_PACKAGE( Threads )
SET(PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
SET(PROJECT_SOURCE_DIR ${CMAKE_CURRENTE_SOURCE_DIR}/src)
INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

SET(COMMON_SRCS_CLIENTE
	./src/MundoCliente.cpp 
	./src/Esfera.cpp
	./src/Plano.cpp
	./src/Raqueta.cpp
	./src/Vector2D.cpp)

SET(COMMON_SRCS_SERVIDOR
	./src/MundoServidor.cpp 
	./src/Esfera.cpp
	./src/Plano.cpp
	./src/Raqueta.cpp
	./src/Vector2D.cpp)

ADD_EXECUTABLE(bot ./src/bot.cpp)
ADD_EXECUTABLE(logger ./src/logger.cpp)
ADD_EXECUTABLE(cliente ./src/cliente.cpp ${COMMON_SRCS_CLIENTE})
ADD_EXECUTABLE(servidor ./src/servidor.cpp ${COMMON_SRCS_SERVIDOR})

TARGET_LINK_LIBRARIES(cliente glut GL GLU)
TARGET_LINK_LIBRARIES(servidor glut GL GLU)
TARGET_LINK_LIBRARIES(servidor ${CMAKE_THREAD_LIBS_INIT})
