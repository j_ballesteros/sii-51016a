//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_CLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO__CLIENTE_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "DatosMemCompartida.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();

    char mensaje[200];
    char cad[200];
    char cadteclas[200];
    int fd;
    int fd_memoria;
    int fd_dibujo; //Fifo para coordenadas//
    int fd_teclas; //Fifo para teclas//

	std::vector <Esfera> ListaEsferas;
	std::vector<Plano> paredes;
    std::vector <Esfera> ListaDisparo1;
    std::vector <Esfera> ListaDisparo2;

    float timer1;
    float timer2;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int puntos1;
	int puntos2;

    DatosMemCompartida Datos;
    DatosMemCompartida *PunteroDatos;
    struct stat bstat;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
