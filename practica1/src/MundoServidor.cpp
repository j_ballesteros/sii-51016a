//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../include/MundoServidor.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
	CMundo* p=(CMundo*) d;
    p->RecibeComandosJugador();
}

CMundo::CMundo()
{
	Init();
	fd=open("/tmp/mififo1",O_WRONLY);
}

CMundo::~CMundo()
{
	close(fd_dibujo);
	close(fd);
	close(fd_teclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	char tipo[20];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
        sprintf(tipo,"SERVIDOR");
        print(tipo,375,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
/// LISTA DE ESFERAS ///////
	for(i=0;i<ListaEsferas.size();i++)
                ListaEsferas[i].Dibuja();
	for(i=0;i<ListaDisparo1.size();i++)
                ListaDisparo1[i].Dibuja();
	for(i=0;i<ListaDisparo2.size();i++)
                ListaDisparo2[i].Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

float contador=0.0f;

void CMundo::OnTimer(int value)
{
	{
		int i;
		int j;
		jugador1.Mueve(0.025f);
		jugador2.Mueve(0.025f);
		for(i=0;i<ListaEsferas.size();i++)
		{
			ListaEsferas[i].Mueve(0.025f);
			jugador1.Rebota(ListaEsferas[i]);
			jugador2.Rebota(ListaEsferas[i]);
			if(fondo_izq.Rebota(ListaEsferas[i]))
			{
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
				puntos2++;
				sprintf(mensaje,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
				write(fd,mensaje,sizeof(mensaje));
			}
			if(fondo_dcho.Rebota(ListaEsferas[i]))
			{
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
				puntos1++;
                sprintf(mensaje,"El jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
                write(fd,mensaje,sizeof(mensaje));
			}
		}
		//esfera.Mueve(0.025f);
		for(i=0;i<paredes.size();i++)
		{
			for(j=0;j<ListaEsferas.size();j++)
				paredes[i].Rebota(ListaEsferas[j]);
			//paredes[i].Rebota(esfera);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}
        for(i=0;i<ListaEsferas.size();i++)
                ListaEsferas[i].Mueve(0.025f);
		for(i=0;i<ListaDisparo1.size();i++)
		{
			ListaDisparo1[i].Mueve(0.025f);
			if(ListaDisparo1[i].centro.x>=7)
			{
				ListaDisparo1[i].~Esfera();
				ListaDisparo1.pop_back();
			}
			if((ListaDisparo1[i].centro.x>=(jugador2.x1+0.5))&&(ListaDisparo1[i].centro.y>(jugador2.y1-0.5))&&(ListaDisparo1[i].centro.y<(jugador2.y2+0.5)))
			{
				ListaDisparo1[i].~Esfera();
				ListaDisparo1.pop_back();
				jugador2.velocidad.y=0;
				timer2=5.0f;
			}
		}
		if(timer2>0.0f)
			timer2=timer2-0.025f;
		else
			timer2=0.0f;
		////////////// Disparo jugador 2 ///////////////
		for(i=0;i<ListaDisparo2.size();i++)
		{
			ListaDisparo2[i].Mueve(0.025f);
			if(ListaDisparo2[i].centro.x<=-7)
			{
				ListaDisparo2[i].~Esfera();
				ListaDisparo2.pop_back();
			}
			if((ListaDisparo2[i].centro.x<=(jugador1.x1+0.5))&&(ListaDisparo2[i].centro.y>(jugador2.y1-0.5))&&(ListaDisparo2[i].centro.y<(jugador1.y2+0.5)))
			{
				ListaDisparo2[i].~Esfera();
				ListaDisparo2.pop_back();
				jugador1.velocidad.y=0;
				timer1=5.0f;
			}
		}
		if(timer1>0.0f)
			timer1=timer1-0.025f;
		else
			timer1=0.0f;
		/////////////// Aumento de las esferas //////////
		contador=contador+0.025f;
		if(contador>=5)
		{
			if(ListaEsferas.size()<=0)
			{
				ListaEsferas.push_back(Esfera());
				contador=0;
			}
		}
	}
	if((puntos1>=8)||(puntos2>=8))
	{
		exit(0);
	}
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f", ListaEsferas[0].centro.x,ListaEsferas[0].centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2, ListaEsferas[0].radio);
	write(fd_dibujo,cad,sizeof(cad));
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
}

void CMundo::Init()
{
	if((fd_dibujo=open("/tmp/fifodibujo",O_WRONLY))<0)perror("NO SE HA PODIDO ABRIR EN EL SERVIDOR EL FIFO DE DAR COORDENADAS");
	if((pthread_create(&thid1, NULL, hilo_comandos, this))<0)perror("ERROR AL CREAR EL HILO RECIBE-TECLAS");
	if((fd_teclas=open("/tmp/fifoteclas",O_RDONLY))<0)perror("NO SE HA PODIDO ABRIR EN EL SERVIDOR EL FIFO DE DAR TECLAS");
	if(ListaEsferas.empty())
		ListaEsferas.push_back(Esfera());
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}


void CMundo::RecibeComandosJugador()
{
	while (1) 
	{
		usleep(10);
		char boton[100];
		read(fd_teclas, boton, sizeof(boton));
		unsigned char key;
		sscanf(boton,"%c",&key);
		if(key=='s')jugador1.velocidad.y=-4;
		if(key=='w')jugador1.velocidad.y=4;
		if(key=='l')jugador2.velocidad.y=-4;
		if(key=='o')jugador2.velocidad.y=4;
	}
}

