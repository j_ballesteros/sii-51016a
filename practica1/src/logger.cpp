#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>

int main()
{
	char salida[200];
	mkfifo("/tmp/mififo1",0777); //Se crea la tuberia //
	int fd=open("/tmp/mififo1",O_RDONLY); //Se abre en modo lectura//
	while(read(fd,salida,sizeof(salida))>0) //Lee en bucle infinito //
	{
		printf("%s",salida);
	}
	close(fd); //Cerramos la tuberia //
	unlink("/tmp/mififo1"); //Se borra la tuberia //
}
